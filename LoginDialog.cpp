#include "LoginDialog.h"

LoginDialog::LoginDialog(QWidget *parent)
   : QDialog(parent)
{
   userLabel = new QLabel("Username:");
   passLabel = new QLabel("Password:");

   username = new QLineEdit;
   password = new QLineEdit;
   password -> setEchoMode(QLineEdit::Password);
   
   login = new QPushButton("Login");
   cancel = new QPushButton("Cancel");
   
   connect(login, SIGNAL(clicked()), this, SLOT(loginClicked()));
   connect(cancel, SIGNAL(clicked()), this, SLOT(close()));
  

   QVBoxLayout *mw = new QVBoxLayout;
   QHBoxLayout *tlayout = new QHBoxLayout;
   QVBoxLayout *labels = new QVBoxLayout;
   QVBoxLayout *lineEdits = new QVBoxLayout;
   QHBoxLayout *btns = new QHBoxLayout;

   labels -> addWidget(userLabel);
   labels -> addWidget(passLabel);
   lineEdits -> addWidget(username);
   lineEdits -> addWidget(password);
   btns -> addWidget(login);
   btns -> addWidget(cancel);
   
   tlayout -> addLayout(labels);
   tlayout -> addLayout(lineEdits);
   mw -> addLayout(tlayout);
   mw -> addLayout(btns);
   setLayout(mw);
   setWindowTitle("Log In");
}

LoginDialog::~LoginDialog()
{
}

void LoginDialog::loginClicked()
{
   if(username -> text() != "" && password -> text() != "")
   {
	emit loginInfoSend(username -> text(), password -> text());
	close();
   }
}
