#include "DatabaseHandler.h"

DatabaseHandler::DatabaseHandler()
{
	index = 0;
	loadData();
	

	//read text file and create array of books

	//* bArray
}

DatabaseHandler::~DatabaseHandler()
{
	for (vector<Book*>::iterator it = bArray.begin(); it != bArray.end(); ++it)
		delete *it;
}

void DatabaseHandler::loadData()
{
	QFile inputFile("bookData.txt");
	if (inputFile.open(QIODevice::ReadOnly))
	{
	   QTextStream in(&inputFile);
	   while (!in.atEnd())
	   {
		QString line = in.readLine();
		QStringList query = line.split(";;");
		
		Book *b = new Book();
		b->setTitle(query[0].toStdString());
		b->setContributor(query[1].toStdString());
		b->setDescription(query[2].toStdString());
		b->setIsbn(query[3].toStdString());
		b->setSpecs(query[4].toStdString());
		b->setCategory(query[5].toStdString());
		b->setImg(query[6].toStdString());


		bArray.push_back(b);
	   }
	   inputFile.close();
	}

	std::cout << bArray.size() << " Books in list" << std::endl;
}

//For testing
void DatabaseHandler::print()
{
	if(!bArray.empty()) std::cout << (bArray.at(0))->getTitle() << std::endl;
	else std::cout << "empty" << std::endl;
}


//For testing
Book* DatabaseHandler::getBook()
{
	Book *b;
	if(!bArray.empty())  b = bArray.at(0);
	else std::cout << "empty" << std::endl;

	return b;
}

Book* DatabaseHandler::getNextBook()
{
	
	if(index >= bArray.size())
	{
		index = 0;
	}		
	Book *b;
	
	b = bArray.at(index++);

	return b;
}

vector<Book*> DatabaseHandler::getBookVector()
{
	return bArray;
}

vector<Book*> DatabaseHandler::getResult(QString keyword, QString field)
{
	
    QRegExp regex(keyword);
	regex.setCaseSensitivity(Qt::CaseInsensitive);

	vector<Book*> bookList;
	for (vector<Book*>::iterator it = bArray.begin(); it != bArray.end(); ++it)
	{
		
		if(field == "Search All: ")
		{
			if(regex.indexIn(QString::fromStdString((*it)->getTitle())) > 0)
			bookList.push_back(*it);
			else if(regex.indexIn(QString::fromStdString((*it)->getCategory())) > 0)
				bookList.push_back(*it);
			else if(regex.indexIn(QString::fromStdString((*it)->getContributor())) > 0)
				bookList.push_back(*it);
		}
		else if(field == "Search Genre: ")
		{
			if(regex.indexIn(QString::fromStdString((*it)->getCategory())) > 0)
				bookList.push_back(*it);
		}
		else if (field == "Search Title: ")
		{
			if(regex.indexIn(QString::fromStdString((*it)->getTitle())) > 0)
			bookList.push_back(*it);
		}
		else if (field == "Search Author: ")
		{
			if(regex.indexIn(QString::fromStdString((*it)->getContributor())) > 0)
				bookList.push_back(*it);
		
		}
	}

	return bookList;
}

