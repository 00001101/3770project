#include "User.h"

User::User()
{
	
}

User::~User()
{

}



void User::addToBookList(Book* book)
{
	bookList.push_back(book);
}

void User::setBookVector(vector<Book*> bv)
{
	bookList = bv;
}


bool User::removeFromBookList(string)
{
	//TODO
}


void User::User::setUsername(string user)
{
	username = user;
}
void User::setPassword(string password)
{
	this->password = password;
}
void User::setFirstName(string name)
{
	firstName = name;
}
void User::setLastName(string name)
{
	lastName = name;
}
void User::setEmail(string email)
{
	this->email = email;
}
void User::setPhone(string phone)
{
	this->phone =  phone;
}
void User::setAddress(string addr)
{
	address = addr;
}
void User::setCity(string city)
{
	this->city = city;
}
void User::setCountry(string count)
{
	country = count;
}
void User::setPostalCode(string postal)
{
	postalCode = postal;
}


string User::getUsername()
{
	return username;
}
string User::getPassword()
{
	return password;
}
string User::getFirstName()
{
	return firstName;
}
string User::getLastName()
{
	return lastName;
}
string User::getEmail()
{
	return email;
}
string User::getPhone()
{
	return phone;
}
string User::getAddress()
{
	return address;
}
string User::getCity()
{
	return city;
}
string User::getCountry()
{
	return country;
}
string User::getPostalCode()
{
	return postalCode;
}

vector<Book*> User::getBookList()
{
	return bookList;
}

