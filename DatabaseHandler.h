#ifndef DATABASE_HANDLER__ 
#define DATABASE_HANDLER__

#include "Book.h"
#include <string>


#include <QFile>
#include <QTextStream>
#include <vector>
#include <QStringList>
#include <iostream>
#include <QRegExp>

class DatabaseHandler
{

	public:
		DatabaseHandler();
		~DatabaseHandler();

		vector<Book*> getBookVector();
		vector<Book*> getResult(QString,QString); //search for books by keyword and return an vector of books

		void loadData();
		void print(); //for testing
		Book* getBook(); //for testing
		Book* getNextBook();
		
		

	private:
		
		int index; 
		

		//number genere 
		//vector<Book*> getBooksByGenre(int);
		//Book[] getBooksByAuther(std::string);
		vector<Book*> bArray;
		
		
		
	
		


};




#endif
