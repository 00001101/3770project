#ifndef TOPNAV_H
#define TOPNAV_H

#include "UserHandler.h"

#include <QtGui>

class TopNavigation : public QWidget
{
	Q_OBJECT

	public:
		TopNavigation(UserHandler*);
		~TopNavigation();


	private:



		QHBoxLayout *layout;
		QPushButton *login;
		QPushButton *reg;
		QPushButton *searchButton;
		QLineEdit *search;
		QComboBox *qbox;
		QLabel *uname;
		QPushButton *logout;
		UserHandler *uh;
		
	signals:
		void regSignal(); //a signal sent when the register button is clicked
		void loginSignal(); //a signal sent when the login button is clicked
		void logoutSignal(); //a signal sent when the logout button is clicked
		void searchSignal(const QString &,const QString &);
		


	public slots:
		void regClicked()
		{
			emit regSignal();
		}
		void loginClicked()
		{
		   emit loginSignal();
		}
		void logoutClicked()
		{
		   emit logoutSignal();
		}
		void serachButtonClicked()
		{
			emit searchSignal(search->text(),qbox->currentText());
		}

};

#endif


