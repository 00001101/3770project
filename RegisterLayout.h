#ifndef REGISTERLAYOUT_H
#define REGISTERLAYOUT_H
#include <QtGui>
#include <string>
#include <iostream>
#include "UserHandler.h"
#include "User.h"

class RegisterLayout : public QWidget
{
	Q_OBJECT

    public:
        RegisterLayout(UserHandler*, int, int);
        ~RegisterLayout();
   

    private:
	
	//Labels
	QLabel 	*emptyLabel;
	QLabel 	*userLabel;
	QLabel  *passLabel;	
	QLabel  *fnLabel;	
    QLabel  *lnLabel;
	QLabel  *emailLabel;
	QLabel	*phoneLabel;
	QLabel 	*addLabel;
	QLabel 	*cityLabel;
	QLabel 	*ctryLabel;
	QLabel 	*pcLabel;

	//Line Edit fields
	QLineEdit *userName;
	QLineEdit *password;	
	QLineEdit *firstName;	
    QLineEdit *lastName;
	QLineEdit *email;
	QLineEdit *phone;
	QLineEdit *dob;
	QLineEdit *address;
	QLineEdit *city;
	QLineEdit *country;
	QLineEdit *postalCode;
	
	//Sign Up Push Button
	QPushButton *signUp;
	UserHandler *uh;
	User *u;
	
	//Layouts
	QFormLayout *formLayout;
	QHBoxLayout *layout;
	

	private slots:
		void signupClicked();
};

#endif


