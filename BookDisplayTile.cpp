#include "BookDisplayTile.h"


 

BookDisplayTile::BookDisplayTile(QWidget *parent, Book* b, UserHandler *uh)
{
	book = b;
	this->uh = uh;

	//Set colors
	QPalette pal;
	 
	// set background to the linear gradient color
	pal.setBrush(QPalette::Background, Qt::white);
	this->setAutoFillBackground(true);
	this->setPalette(pal);


	//set height and width
	this->setMaximumHeight(500);
	this->setMinimumHeight(215);
	this->setMaximumWidth(260);
	this->setMinimumWidth(260);
	

	layout = new QVBoxLayout();

	title = new QLabel(this);
	title->setWordWrap(true);
	title->setText(QString::fromStdString(b->getTitle()));
	contributor = new QLabel(this);
	contributor->setText(QString::fromStdString(b->getContributor()));
	desc = new QLabel(this);
	desc->setWordWrap(true);
	std::string d = b->getDescription();
	if(d.length() > 200) d = b->getDescription().substr(0,200) + "...";
	desc->setText(QString::fromStdString(d));

	//Image
	QPixmap image(QString::fromStdString("images/"+b->getImg()));

	imageLabel = new QLabel();
	imageLabel->setPixmap(image);
	imageLabel->setScaledContents(true);
	
	checkoutButton = new QPushButton("Check out");
	
	

	//Add to widget
	layout->addWidget(title);
	layout->addWidget(contributor);
	layout->addWidget(imageLabel);
	layout->addWidget(desc);

	if(uh->isLoggedIn()) layout->addWidget(checkoutButton);
	
	this->setLayout(layout);
	
	connect(checkoutButton, SIGNAL(clicked()), this, SLOT(checkout()));
}

void BookDisplayTile::checkout()
{
	uh->getUser()->addToBookList(book);
	std::cout << "added?" << std::endl;
}

BookDisplayTile::~BookDisplayTile()
{
	
}





Book* BookDisplayTile::getBook()
{
	return book;
}
