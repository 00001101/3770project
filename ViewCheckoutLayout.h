#ifndef VIEWCHECKOUTLAYOUT_H
#define VIEWCHECKOUTLAYOUT_H


#include <QtGui>
#include "UserHandler.h"
#include <iostream>

class ViewCheckoutLayout : public QDialog
{
	Q_OBJECT

	public:
		ViewCheckoutLayout(int, int,UserHandler*);
		~ViewCheckoutLayout();


	private:
		QTableWidget *table;

		QPushButton *returnB;
		QVBoxLayout *vbox;

};

#endif

