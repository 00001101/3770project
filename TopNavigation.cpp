#include "TopNavigation.h"


TopNavigation::TopNavigation(UserHandler *uh)
{
	this->uh = uh;


	QPalette Pal;

	QLinearGradient linearGradient(0,0,0,70);
	linearGradient.setColorAt(0, QColor(192, 192, 192, 90));
	linearGradient.setColorAt(0.5, QColor(100, 100, 100, 100));
	
	// set black background
	Pal.setBrush(QPalette::Background, QBrush(linearGradient));
	this->setAutoFillBackground(true);
	this->setPalette(Pal);

	
	this->setMaximumHeight(70);
	this->setMinimumHeight(70);
	
	
	layout = new QHBoxLayout;

	login = new QPushButton;
	logout = new QPushButton;
	reg = new QPushButton;

	if(!uh->isLoggedIn())
	{
		login->setText("Log In");
		reg->setText("Register!");
	}
	else
	{
		logout->setText("Logout");
		uname = new QLabel(QString::fromStdString(uh->getUser()->getUsername()));
	}
	search = new QLineEdit;
	qbox = new QComboBox();
	qbox->addItem("Search All: ");
	qbox->addItem("Search Genre: ");
	qbox->addItem("Search Title: ");
	qbox->addItem("Search Author: ");
	
	
	
	
	search->setText("Search by Title or Keyword");
	search->setMaximumWidth(250);
	
	searchButton = new QPushButton;
	searchButton->setText("Search");
	
	
	QWidget* empty = new QWidget();
	empty->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
	
	//adding widgets to the layout
	
	layout->addWidget(qbox);
	layout->addWidget(search);
	layout->addWidget(searchButton);
	layout->addWidget(empty);
	if(!uh->isLoggedIn())
	{
		layout->addWidget(login);
		layout->addWidget(reg);
	}
	else
	{
		layout->addWidget(uname);
		layout->addWidget(logout);
	}


	this->setLayout(layout);

	connect(reg, SIGNAL(clicked()), this, SLOT(regClicked()));
	connect(login, SIGNAL(clicked()), this, SLOT(loginClicked()));
	connect(logout, SIGNAL(clicked()), this, SLOT(logoutClicked()));
	connect(searchButton, SIGNAL(clicked()), this, SLOT(serachButtonClicked()));

	
}

TopNavigation::~TopNavigation()
{
	
}
