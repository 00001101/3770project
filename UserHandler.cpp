#include "UserHandler.h"



UserHandler::UserHandler(DatabaseHandler* dbh)
{
	this->dbh = dbh;
	user = NULL;
	loadData();
}

UserHandler::~UserHandler()
{
	
	QFile file("userData.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
	
 
	
	
	for (vector<User*>::iterator it = uVector.begin(); it != uVector.end(); ++it)
	{
		
		User* us = *it;
		QString output = 	QString::fromStdString(us->getUsername()) + ";" +
							QString::fromStdString(us->getPassword()) + ";" +
							QString::fromStdString(us->getFirstName()) + ";" +
							QString::fromStdString(us->getLastName()) + ";" +
							QString::fromStdString(us->getEmail()) + ";" +
							QString::fromStdString(us->getPhone()) + ";" +
							QString::fromStdString(us->getAddress()) + ";" +
							QString::fromStdString(us->getCity()) + ";" +
							QString::fromStdString(us->getCountry()) + ";" +
							QString::fromStdString(us->getPostalCode()) + ";";

		vector<Book*> bl = us->getBookList();
		for (vector<Book*>::iterator it1 = bl.begin(); it1 != bl.end(); ++it1)
		{
			output += QString::fromStdString((*it1)->getTitle()) + ",";
		}
		out << output <<  "\n";

	}

	file.flush();
	file.close();
	
	//Persist current users first

	for (vector<User*>::iterator it = uVector.begin(); it != uVector.end(); ++it)
		delete *it;
}


bool UserHandler::isLoggedIn()
{
	if(user == NULL)
	{
		return false;
	}
	return true;
}

User* UserHandler::getUser()
{
	return user;
}


bool UserHandler::checkUserPass(string usern, string pass)
{

	for (vector<User*>::iterator it = uVector.begin(); it != uVector.end(); ++it)
	{

		if((*it)->getUsername() == usern)
		{

			if((*it)->getPassword() == pass)
			{
				this->user = (*it);
				return true;
			}
		}
	}
	return false;
}

void UserHandler::loadData()
{
	
	QFile inputFile("userData.txt");
	if (inputFile.open(QIODevice::ReadOnly))
	{
	   QTextStream in(&inputFile);
	   while (!in.atEnd())
	   {
		QString line = in.readLine();
		QStringList query = line.split(";");
		
		User *u = new User();
		u->setUsername(query[0].toStdString());
		u->setPassword(query[1].toStdString());
		u->setFirstName(query[2].toStdString());
		u->setLastName(query[3].toStdString());
		u->setEmail(query[4].toStdString());
		u->setPhone(query[5].toStdString());
		u->setAddress(query[6].toStdString());
		u->setCity(query[7].toStdString());
		u->setCountry(query[8].toStdString());
		u->setPostalCode(query[9].toStdString());

		QStringList books = query[9].split(",");

		vector<Book*> bv;
		for(int i=0; i < books.size(); i++)
		{
			dbh->getNextBook();
			bv.push_back(dbh->getNextBook());
			dbh->getNextBook();
			bv.push_back(dbh->getNextBook());
			bv.push_back(dbh->getNextBook());
			dbh->getNextBook();
			bv.push_back(dbh->getNextBook());
		}
		u->setBookVector(bv);
		uVector.push_back(u);
	   }
	   inputFile.close();
	}
}

void UserHandler::logout()
{
	user = NULL;
}

void UserHandler::addUser(User* u)
{
	uVector.push_back(u);
	
}


