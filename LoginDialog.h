#ifndef LOGIN_DIALOG_H
#define LOGIN_DIALOG_H
#include <QtGui>

class LoginDialog : public QDialog
{
        Q_OBJECT
      
    public:
	LoginDialog(QWidget *parent = 0);
        ~LoginDialog();

    private slots:
	void loginClicked();

    signals:
	void loginInfoSend(const QString &, const QString &);
	
    private:
	QDialog *dialog;
	QLabel *userLabel;
	QLabel *passLabel;
	QLineEdit *username;
	QLineEdit *password;
	QPushButton *login;
	QPushButton *cancel;
};

#endif
