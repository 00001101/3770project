#ifndef UPDATEINFO_H
#define UPDATEINFO_H
#include <QtGui>
#include <string>
#include "UserHandler.h"
#include "User.h"

class UpdateInfo : public QWidget
{
	Q_OBJECT

	public:
		UpdateInfo(UserHandler*,int, int);
		~UpdateInfo();

	private:
		QHBoxLayout *layout;

		QLabel 	*emptyLabel;
		QLabel  *passLabel;
		QLabel  *confirmPassLabel;
		QLabel  *fnLabel;	
		QLabel  *lnLabel;
		QLabel  *emailLabel;
		QLabel	*phoneLabel;
		QLabel 	*addLabel;
		QLabel 	*cityLabel;
		QLabel 	*ctryLabel;
		QLabel 	*pcLabel;

		QLineEdit *password;
		QLineEdit *confirmPass;
		QLineEdit *firstName;	
		QLineEdit *lastName;
		QLineEdit *email;
		QLineEdit *phone;
		QLineEdit *dob;
		QLineEdit *address;
		QLineEdit *city;
		QLineEdit *country;
		QLineEdit *postalCode;

		QPushButton *updateB;

		UserHandler *uh;
		User *user;

	private slots:
		void updateClicked();
};

#endif
