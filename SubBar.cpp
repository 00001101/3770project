#include "SubBar.h"

SubBar::SubBar(QWidget* parent)
{
	homeL = new MyLabel(QString::fromStdString("Home"),this);
	updateInfoL = new MyLabel(QString::fromStdString("Update Info"),this);
	viewOrdersL = new MyLabel(QString::fromStdString("View Checkout List"),this);

	homeL->setAlignment(Qt::AlignCenter);
	updateInfoL->setAlignment(Qt::AlignCenter);
	viewOrdersL->setAlignment(Qt::AlignCenter);

	QPalette Pal;

	QLinearGradient linearGradient(0,0,0,70);
	linearGradient.setColorAt(0, QColor(218, 218, 218, 100));
	linearGradient.setColorAt(0.5, QColor(218, 218, 218, 100));
	 
	// set background to the linear gradient color
	Pal.setBrush(QPalette::Background, QBrush(linearGradient));
	this->setAutoFillBackground(true);
	this->setPalette(Pal);

	this->setMaximumHeight(70);
	this->setMinimumHeight(70);
		
	layout = new QHBoxLayout;
	
	
	//Create a new QWidget

	QWidget* empty = new QWidget();
	empty->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);

	//adding widgets to the layout
	
	layout->addWidget(homeL);
	layout->addWidget(updateInfoL);
	layout->addWidget(viewOrdersL);

	this->setLayout(layout);

	connect(homeL, SIGNAL(clicked()), this, SLOT(homeClicked()));
	connect(updateInfoL, SIGNAL(clicked()), this, SLOT(updateClicked()));
	connect(viewOrdersL, SIGNAL( clicked() ), this, SLOT(bookCheckClicked()));
}

void SubBar::homeClicked()
{
	std::cout << "home subbar clicked\n";	
	emit mainWindowHomeClicked();
}

void SubBar::updateClicked()
{
	std::cout << "update subbar clicked\n";	
	emit mainWindowUpdateClicked();
}

void SubBar::bookCheckClicked()
{
	std::cout << "checkout subbar clicked\n";	
	emit mainWindowCheckClicked();
}

SubBar::~SubBar()
{
	
}
