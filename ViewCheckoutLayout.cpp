#include "ViewCheckoutLayout.h"

ViewCheckoutLayout::ViewCheckoutLayout(int maxHeight, int minHeight, UserHandler* uh) 
{	

	this->setMaximumHeight(maxHeight);
	
	vector<Book*> bookList = uh->getUser()->getBookList();
	
	vbox = new QVBoxLayout;

	table = new QTableWidget;
	table->setRowCount(bookList.size());

	table->setColumnCount(4);
	int index =0;
	for (vector<Book*>::iterator it = bookList.begin(); it != bookList.end(); ++it)
	{
		std::cout << index << std::endl;
		Book* b = *it;
		
		table->setCellWidget(index, 0, new QLabel(QString::fromStdString(b->getTitle())));
		
		table->setCellWidget(index, 1, new QLabel(QString::fromStdString(b->getContributor())));
		table->setCellWidget(index, 2, new QLabel(QString::fromStdString(b->getSpecs())));
		table->setCellWidget(index, 3, new QLabel(QString::fromStdString(b->getCategory())));
		index++;
	
	}

	
	table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
	vbox->addWidget(table);
	setLayout(vbox);
	

}


ViewCheckoutLayout::~ViewCheckoutLayout()
{
	delete table;
}

