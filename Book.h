#ifndef BOOK_H
#define BOOK_H

#include <Book.h>
#include <string>


using namespace std;

class Book
{

	public:
		Book();
		~Book();

		void setTitle(string);
		void setContributor(string);
		void setDescription(string);
		void setIsbn(string);
		void setSpecs(string);
		void setCategory(string);
		void setImg(string);


		string getTitle();
		string getContributor();
		string getDescription();
		string getIsbn();
		string getSpecs();
		string getCategory();
		string getImg();

	

	private:
		
		string title;
		string contributor;
		string description;
		string isbn;
		string specs;
		string category;
		string img;
	
		
		
};

#endif


