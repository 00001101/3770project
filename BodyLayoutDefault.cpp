#include "BodyLayoutDefault.h"


BodyLayoutDefault::BodyLayoutDefault(int maxHeight, int minHeight, DatabaseHandler *dbh, UserHandler *uh)
{
	this->dbh = dbh;
	this->uh = uh;

	
    layout = new QVBoxLayout(this);

    qsa = new QScrollArea(this);
    
    qsa->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    qsa->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    qsa->setWidgetResizable(false);
    
    gridLayout = new QGridLayout();
	gridLayout->setHorizontalSpacing(15);
	gridLayout->setVerticalSpacing(15);
    container = new QWidget();
	container->setMaximumHeight(maxHeight);
	container->setMinimumHeight(minHeight);
	container->setLayout(gridLayout);
    gridLayout->setSizeConstraint(QLayout::SetFixedSize);
    gridLayout->setAlignment(Qt::AlignCenter);
    qsa->setWidget(container);

    layout->addWidget(qsa);

	
	
	//gridLayout->addWidget(new QPushButton);

    //this->setCentralWidget(mainWidget);
}

void BodyLayoutDefault::renderBooks()
{
	//gridLayout->addWidget(new BookDisplayTile(this, dbh->getNextBook()), i,j,0);

	for(int i=0; i < 5; i++)
	{
		for(int j=0; j < 5; j++)
		{
			gridLayout->addWidget(new BookDisplayTile(this, dbh->getNextBook(),uh), i,j,Qt::AlignHCenter);
		}
		
	}
}

void BodyLayoutDefault::renderBooks(QString keyword, QString field)
{
	vector<Book*> bv = dbh->getResult(keyword, field);
	
	int totalBooks = bv.size();
	
	int height = ceil(totalBooks/5);
	int t=0;
	
	for(int i=0; i <= height; i++)
	{
		for(int j=0; j < 5 && t < totalBooks; j++)
		{
			Book *b = bv.at(t++);
			BookDisplayTile *bdt = new BookDisplayTile(this, b,uh);
			gridLayout->addWidget(bdt, i,j,Qt::AlignHCenter);
	
			bdtv.push_back(bdt);
		}

	}
	std::cout << "found: " << bv.size() << std::endl;
}


BodyLayoutDefault::~BodyLayoutDefault()
{
	for (vector<BookDisplayTile*>::iterator it = bdtv.begin(); it != bdtv.end(); ++it)
		delete *it;
}

/*

	
	this->setObjectName("bg");
	QFile file("BodyLayoutDefault.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	this->setStyleSheet(styleSheet);

	*/

	
	
	
	
