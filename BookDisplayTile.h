#ifndef __BOOK_DISPLAY_TILE__H
#define __BOOK_DISPLAY_TILE__H

#include "Book.h"
#include <QtGui>
#include <iostream>
#include "UserHandler.h"

class BookDisplayTile : public QWidget
{
	 Q_OBJECT


	public:
		BookDisplayTile(QWidget *parent, Book*, UserHandler*);
		~BookDisplayTile();
		Book* getBook();



	private:
		Book *book;
		QVBoxLayout *layout;
		QLabel *title;
		QLabel *contributor;
		QLabel *imageLabel;
		QLabel *desc;
		QPushButton *checkoutButton;
		UserHandler *uh;

		
		
	private slots:
		void checkout();
		

};



#endif
