#include "UpdateInfo.h"

UpdateInfo::UpdateInfo(UserHandler *uh, int maxHeight, int minHeight)
{
	this->uh = uh;

	QFile file("BodyLayoutDefault.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	this->setObjectName("bg");
	
	

	this->setMaximumHeight(maxHeight);
	//this->setMinimumHeight(minHeight);
	
	layout = new QHBoxLayout;
	QFormLayout *formLayout = new QFormLayout;
	QWidget *centreForm = new QWidget;
	QWidget *spacer1 = new QWidget;
	QWidget *spacer2 = new QWidget;
	centreForm->setMaximumWidth(450);

        emptyLabel = new QLabel("");
	passLabel = new QLabel("Password: ");
	confirmPassLabel = new QLabel("Confirm Password: ");
	fnLabel = new QLabel("First Name: ");	
        lnLabel = new QLabel("Last Name: ");
	emailLabel = new QLabel("Email: ");
	phoneLabel = new QLabel("Phone: ");
	addLabel = new QLabel("Address: ");
	cityLabel = new QLabel("City: ");
	ctryLabel = new QLabel("Country: ");
	pcLabel = new QLabel("Postal Code: ");

	password = new QLineEdit;
	confirmPass = new QLineEdit;
	firstName = new QLineEdit;
	lastName = new QLineEdit;
	email = new QLineEdit;
	phone = new QLineEdit;
	address = new QLineEdit;
	city = new QLineEdit;
	country = new QLineEdit;
	postalCode = new QLineEdit;
	
	password->setEchoMode(QLineEdit::Password);
	confirmPass->setEchoMode(QLineEdit::Password);

	updateB = new QPushButton;
	updateB->setText("Update");
	
	user = uh->getUser();
	password->setText(QString::fromStdString(user->getPassword()));
	confirmPass->setText(QString::fromStdString(user->getPassword()));
	firstName->setText(QString::fromStdString(user->getFirstName()));
	lastName->setText(QString::fromStdString(user->getLastName()));
	email->setText(QString::fromStdString(user->getEmail()));
	phone->setText(QString::fromStdString(user->getPhone()));
	address->setText(QString::fromStdString(user->getAddress()));
	city->setText(QString::fromStdString(user->getCity()));
	country->setText(QString::fromStdString(user->getCountry()));
	postalCode->setText(QString::fromStdString(user->getPostalCode()));

	this->setStyleSheet(styleSheet);

	formLayout->addRow(passLabel, password);
	formLayout->addRow(confirmPassLabel, confirmPass);
	formLayout->addRow(fnLabel, firstName);
	formLayout->addRow(lnLabel, lastName);
	formLayout->addRow(emailLabel, email);
	formLayout->addRow(phoneLabel, phone);
	formLayout->addRow(addLabel, address);
	formLayout->addRow(cityLabel, city);
	formLayout->addRow(ctryLabel, country);
	formLayout->addRow(pcLabel, postalCode);
	formLayout->addRow(emptyLabel, updateB);	
	
	centreForm->setLayout(formLayout);
	layout->addWidget(spacer1);
	layout->addWidget(centreForm);
	layout->addWidget(spacer2);
	this->setLayout(layout);

	connect(updateB, SIGNAL(clicked()), this, SLOT(updateClicked()));
}

void UpdateInfo::updateClicked()
{
	if(password->text() == confirmPass->text())
	{
		user->setPassword(password->text().toStdString());
		user->setFirstName(firstName->text().toStdString());
		user->setLastName(lastName->text().toStdString());
		user->setEmail(email->text().toStdString());
		user->setPhone(phone->text().toStdString());
		user->setAddress(address->text().toStdString());
		user->setCity(city->text().toStdString());
		user->setCountry(country->text().toStdString());
		user->setPostalCode(postalCode->text().toStdString());
		
		QMessageBox successBox;
		successBox.information(0, "Success", "Success! Info Updated.");
		successBox.setFixedSize(500,200);
		successBox.exec();
	}
	else
	{
		QMessageBox errorBox;
        errorBox.critical(0,"Error","Password fields do not match! Try again.");
        errorBox.setFixedSize(500,200);
		errorBox.exec();
	}
}

UpdateInfo::~UpdateInfo()
{
	
}
