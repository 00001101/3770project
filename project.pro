######################################################################
# Automatically generated by qmake (2.01a) Tue Apr 5 00:47:44 2016
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += BodyLayoutDefault.h \
           Book.h \
           BookDisplayTile.h \
           DatabaseHandler.h \
           LoginDialog.h \
           MainWindow.h \
           MyLabel.h \
           RegisterLayout.h \
           SubBar.h \
           TopNavigation.h \
           UpdateInfo.h \
           User.h \
           UserHandler.h \
           ViewCheckoutLayout.h
SOURCES += BodyLayoutDefault.cpp \
           Book.cpp \
           BookDisplayTile.cpp \
           DatabaseHandler.cpp \
           LoginDialog.cpp \
           main.cpp \
           MainWindow.cpp \
           MyLabel.cpp \
           RegisterLayout.cpp \
           SubBar.cpp \
           TopNavigation.cpp \
           UpdateInfo.cpp \
           User.cpp \
           UserHandler.cpp \
           ViewCheckoutLayout.cpp
