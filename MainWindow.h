#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "TopNavigation.h"
#include "Book.h"
#include "User.h"
#include "BodyLayoutDefault.h"
#include "RegisterLayout.h"
#include "User.h"
#include "SubBar.h"
#include "DatabaseHandler.h"
#include "UpdateInfo.h"
#include "UserHandler.h"
#include "LoginDialog.h"
#include "ViewCheckoutLayout.h"


#include <QtGui>
#include <iostream>

class MainWindow : public QMainWindow
{
	Q_OBJECT
	public:
		MainWindow();
		~MainWindow();
	
	private:
		TopNavigation *nav;
		QVBoxLayout *mainLayout;
		BodyLayoutDefault * bld;
		RegisterLayout *rl;
		SubBar *sb;
		DatabaseHandler *dbh;
		UpdateInfo *ui;
		UserHandler *uh;
		LoginDialog *ld;
		ViewCheckoutLayout *vcl;
		
		
		void cleanUpLayout();

		int height;
		int width;
		
		void renderSearch(QString, QString);
		void renderDefaultView();	//Default Main Window
		//void renderUserView();		//Displays User Information and Books checked out by the user	
		//void bookView(Book b);		//Displays the list of book by the search query and if logged in can checkout a book
		void registerView();	//Displays the register screen where a user can register
		void updateInfoView(); //Displays the window where user can update his personal information
		void bookCheckoutView();	


		bool activeUser();

	
	public slots:
		void regSlot();
		void homeSlot();
		void updateSlot();
		void checkoutSlot();
		void loginSlot();
		void receiveLogin(const QString &, const QString &);
		void logoutSlot();
		void receiveSearch(const QString &, const QString &);
};

#endif
