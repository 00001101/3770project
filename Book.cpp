#include "Book.h"

Book::Book()
{
	
}

Book::~Book()
{

}


void Book::setTitle(string title)
{
	this->title = title;
}
void Book::setContributor(string con)
{
	this->contributor = con;
}
void Book::setDescription(string desc)
{
	this->description = desc;
}
void Book::setIsbn(string isbn)
{
	this->isbn = isbn;
}
void Book::setSpecs(string specs)
{
	this->specs = specs;
}
void Book::setCategory(string cate)
{
	this->category = cate;
}
void Book::setImg(string img)
{
	this->img = img;
}


string Book::getTitle()
{
	return title;
}
string Book::getContributor()
{
	return contributor;
}
string Book::getDescription()
{
	return description;
}
string Book::getIsbn()
{
	return isbn;
}
string Book::getSpecs()
{
	return specs;
}
string Book::getCategory()
{
	return category;
}
string Book::getImg()
{
	return img;
}

