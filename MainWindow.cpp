#include "MainWindow.h"

MainWindow::MainWindow()
{
	dbh = new DatabaseHandler();
	uh = new UserHandler(dbh);

	QRect rec = QApplication::desktop()->screenGeometry();
	height = rec.height();
	width = rec.width();



	// uh->checkUserPass("stone1652", "password");

	/**
	QFile file("style.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	cWidget->setStyleSheet(styleSheet);
	**/



	QWidget *cWidget = new QWidget();
	setCentralWidget(cWidget);
	//cWidget->setStyleSheet(styleSheet);



	nav = new TopNavigation(uh);
	sb = new SubBar(this);
	mainLayout = new QVBoxLayout;
	

	cWidget->setLayout(mainLayout);
	mainLayout->setContentsMargins(0, 0, 0, 0);

	this->resize(width,height-70);
	this->move(QPoint(0,0));


	mainLayout->addWidget(nav);
	mainLayout->addWidget(sb);

	bld = NULL;
	rl = NULL;
	vcl = NULL;
	ui = NULL;

	renderDefaultView();
	//bookCheckoutView();

	connect(nav, SIGNAL(regSignal()), this, SLOT(regSlot()));

	connect(sb, SIGNAL(mainWindowHomeClicked()),this,SLOT(homeSlot()));

	
	connect(sb, SIGNAL(mainWindowUpdateClicked()),this,SLOT(updateSlot()));
	connect(sb, SIGNAL(mainWindowCheckClicked()),this,SLOT(checkoutSlot()));

	

	connect(nav, SIGNAL(loginSignal()), this, SLOT(loginSlot()));
	
	connect(nav, SIGNAL(logoutSignal()), this, SLOT(logoutSlot()));

	connect(nav, SIGNAL(searchSignal(const QString &, const QString &)), this, SLOT(receiveSearch(const QString &, const QString &)));
}


MainWindow::~MainWindow()
{
	delete uh;	
	delete dbh;
	
}




bool MainWindow::activeUser()
{
	return uh->isLoggedIn();
}

void MainWindow::regSlot()
{
	registerView();
}

void MainWindow::homeSlot()
{
	std::cout << "home main clicked\n";	
	renderDefaultView();
}

void MainWindow::updateSlot()
{
	std::cout << "update main clicked\n";		
	updateInfoView();
}

void MainWindow::checkoutSlot()
{
	std::cout << "checkout main clicked\n";		
	bookCheckoutView();
}

void MainWindow::loginSlot()
{
	ld = new LoginDialog(this);
	ld -> setModal(true);
	connect(ld, SIGNAL(loginInfoSend(const QString &, const QString &)), this, SLOT(receiveLogin(const QString &, const QString &)));
	ld -> show();
	ld -> activateWindow();

}

void MainWindow::receiveSearch(const QString & keyword, const QString & field)
{
	renderSearch(keyword, field);
}


void MainWindow::receiveLogin(const QString &user, const QString &pass)
{
	
	if(!uh->checkUserPass(user.toStdString(), pass.toStdString()))
	{
		QMessageBox messageBox;
        messageBox.critical(0,"Error","Invalid Login! Try again.");
        messageBox.setFixedSize(500,200);
		messageBox.exec();
		
	}
	
	cleanUpLayout();
	mainLayout->removeWidget(nav);
	delete nav;
	nav = new TopNavigation(uh);
	connect(nav, SIGNAL(logoutSignal()), this, SLOT(logoutSlot()));
	connect(nav, SIGNAL(regSignal()), this, SLOT(regSlot()));
	connect(nav, SIGNAL(loginSignal()), this, SLOT(loginSlot()));
	connect(sb, SIGNAL(mainWindowUpdateClicked()),this,SLOT(updateSlot()));
	connect(sb, SIGNAL(mainWindowCheckClicked()),this,SLOT(checkoutSlot()));
	
	//Line that was fixed
	connect(nav, SIGNAL(searchSignal(const QString &, const QString &)), this, SLOT(receiveSearch(const QString &, const QString &)));
	//end fix
	
	mainLayout->removeWidget(sb);
	mainLayout->addWidget(nav);
	mainLayout->addWidget(sb);
	renderDefaultView();
}

void MainWindow::logoutSlot()
{
	uh -> logout();
	
	cleanUpLayout();
	mainLayout->removeWidget(nav);
	delete nav;
	nav = new TopNavigation(uh);
	connect(nav, SIGNAL(loginSignal()), this, SLOT(loginSlot()));
	connect(nav, SIGNAL(regSignal()), this, SLOT(regSlot()));
	
	//Also fixed to update
	connect(nav, SIGNAL(searchSignal(const QString &, const QString &)), this, SLOT(receiveSearch(const QString &, const QString &)));
	//end fix 2
	
	mainLayout->removeWidget(sb);
	mainLayout->addWidget(nav);
	mainLayout->addWidget(sb);
	renderDefaultView();
}

void MainWindow::cleanUpLayout()
{
		if(rl != NULL)
		{
			mainLayout->removeWidget(rl);
			delete rl;
			rl = NULL;
		}
		if(bld != NULL)
		{
			mainLayout->removeWidget(bld);
			delete bld;
			bld = NULL;
		}
		if(ui != NULL)
		{
			mainLayout->removeWidget(ui);
			delete ui;
			ui = NULL;
		}
		if(vcl != NULL)
		{
			mainLayout->removeWidget(vcl);
			delete vcl;
			vcl = NULL;
		}
		update();
}

void MainWindow::renderDefaultView()
{
	std::cout << "RenderDefaultView" << std::endl;
	if(bld == NULL)
	{
		cleanUpLayout();
		
		bld = new BodyLayoutDefault(height-70, height-70, dbh, uh);
		bld->renderBooks();
		mainLayout->addWidget(bld);
	}
}

void MainWindow::renderSearch(QString ss, QString sf)
{
	std::cout << "Searching: " << sf.toStdString() << std::endl;
	
	cleanUpLayout();
	
	if (ss.toStdString() == "")
	{
		bld = new BodyLayoutDefault(height-70, height-70, dbh, uh);
		bld->renderBooks();
		mainLayout->addWidget(bld);
		std::cout << "finished search\n";
	}
	else
	{		
		bld = new BodyLayoutDefault(height-70, height-70, dbh, uh);
		bld->renderBooks(ss,sf);
		mainLayout->addWidget(bld);
		std::cout << "finished search: " << ss.toStdString() << std::endl;
	}
}

void MainWindow::registerView()
{
	std::cout << "RegisterView" << std::endl;
	if(rl == NULL)
	{
		cleanUpLayout();
		
		rl = new RegisterLayout(uh, height-70, height-70);
		mainLayout->addWidget(rl);
	}
}

void MainWindow::updateInfoView()
{
	std::cout << "UpdateInfoView" << std::endl;

	if(!uh->isLoggedIn())
	{
		QMessageBox messageBox;
        messageBox.critical(0,"Error","Please Login!");
		messageBox.exec();

		return;
	}
	
	if(ui == NULL)
	{
		cleanUpLayout();	
		
		ui = new UpdateInfo(uh, height-70, height-70);
		mainLayout->addWidget(ui);
	}	
}

void MainWindow::bookCheckoutView()
{
	std::cout << "BookCheckoutView" << std::endl;
	
	if(!uh->isLoggedIn())
	{
		QMessageBox messageBox;
        messageBox.critical(0,"Error","Please Login!");
		messageBox.exec();

		return;
	}
	if(vcl == NULL)
	{
		cleanUpLayout();	
		vcl = new ViewCheckoutLayout(height-70, height-70, uh);
		mainLayout->addWidget(vcl);
	}
}

