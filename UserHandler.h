#ifndef __USER_HANDLER__H
#define __USER_HANDLER__H


#include "User.h"
#include "DatabaseHandler.h"


#include <vector>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <iostream>

class UserHandler
{

	public:

		UserHandler(DatabaseHandler*);
		~UserHandler();

		void addUser(User*);
		void logout();

		bool checkUserPass(string, string);
		bool isLoggedIn();
		User* getUser();

	private:

		User *user;
		vector<User*> uVector;
		void loadData();
		DatabaseHandler* dbh;

		
		

};






#endif
