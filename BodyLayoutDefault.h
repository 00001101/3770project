#ifndef BODYLAYOUTDEFAULT_H
#define BODYLAYOUTDEFAULT_H

#include <QtGui>
#include "DatabaseHandler.h"
#include "BookDisplayTile.h"
#include "Book.h"
#include <math.h>
#include <iostream>

class BodyLayoutDefault : public QWidget
{
	Q_OBJECT

    public:
        BodyLayoutDefault(int, int, DatabaseHandler*, UserHandler*);
        ~BodyLayoutDefault();
		void renderBooks(QString,QString);
		void renderBooks();
		
    private:
		QVBoxLayout *layout;
		QGridLayout *gridLayout;
		QScrollArea *qsa;
		QWidget *container;

		DatabaseHandler *dbh;
		UserHandler *uh;

		vector<BookDisplayTile*> bdtv;

};

#endif


