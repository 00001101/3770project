#ifndef SUBBAR_H
#define SUBBAR_H

#include "MyLabel.h"
#include <QtGui>
#include <iostream>

class SubBar : public QWidget
{
	Q_OBJECT

	public:
		SubBar(QWidget* parent);
		~SubBar();


	private:
		QHBoxLayout *layout;
		MyLabel *homeL;
		MyLabel *updateInfoL;
		MyLabel *viewOrdersL;


	signals:
		void mainWindowHomeClicked(); //a signal sent when the home label is clicked
		void mainWindowUpdateClicked(); //a signal sent when the update label is clicked
		void mainWindowCheckClicked(); //a signal sent when the view label is clicked

	public slots:
		void homeClicked();	
		void updateClicked();
		void bookCheckClicked();
};

#endif


