#ifndef USER_H
#define USER_H

#include <string>
#include <vector>
#include "Book.h"

using namespace std;

class User
{
	public:
		User();
		~User();


		void addToBookList(Book*);
		bool removeFromBookList(string);
		void setBookVector(vector<Book*>);



		void setUsername(string);
		void setPassword(string);
		void setFirstName(string);
		void setLastName(string);
		void setEmail(string);
		void setPhone(string);
		void setAddress(string);
		void setCity(string);
		void setCountry(string);
		void setPostalCode(string);
		

		string getUsername();
		string getPassword();
		string getFirstName();
		string getLastName();
		string getEmail();
		string getPhone();
		string getAddress();
		string getCity();
		string getCountry();
		string getPostalCode();
		
		vector<Book*> getBookList();

	private:
		string username;
		string password;
		string firstName;
		string lastName;
		string email;
		string phone;
		string address;
		string city;
		string country;	
		string postalCode;


		vector<Book*> bookList;
		
		

};

#endif


