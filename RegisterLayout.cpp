#include "RegisterLayout.h"

RegisterLayout::RegisterLayout(UserHandler *uh, int maxHeight, int minHeight)
{
	this->uh = uh;

	QFile file("BodyLayoutDefault.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	this->setObjectName("bg");
	
	u = new User;

	this->setMaximumHeight(maxHeight);
	//this->setMinimumHeight(minHeight);
	
	// instantiate layout
	
	formLayout = new QFormLayout;
	layout = new QHBoxLayout;

	QWidget *centreForm = new QWidget;
	
	QWidget *space1 = new QWidget;
	QWidget *space2 = new QWidget;
	
	centreForm->setMaximumWidth(450);
	
	// labels for the fields
	userLabel = new QLabel("User Name: ");
	//userLabel->setAlignment(Qt::AlignRight);
	passLabel = new QLabel("Password: ");	
	fnLabel = new QLabel("First Name: ");	
    lnLabel = new QLabel("Last Name: ");
	emailLabel = new QLabel("Email: ");
	phoneLabel = new QLabel("Phone: ");
	addLabel = new QLabel("Address: ");
	cityLabel = new QLabel("City: ");
	ctryLabel = new QLabel("Country: ");
	pcLabel = new QLabel("Postal Code: ");
	emptyLabel = new QLabel("");


	// fields where the user will enter information to register
	userName = new QLineEdit;
	//userName->setAlignment(Qt::AlignLeft);
	userName->setMaximumWidth(450);

	password = new QLineEdit;
	password->setMaximumWidth(450);
	password->setEchoMode(QLineEdit::Password);

	firstName = new QLineEdit;
	firstName->setMaximumWidth(450);

	lastName = new QLineEdit;
	lastName->setMaximumWidth(450);

	email = new QLineEdit;
	email->setMaximumWidth(450);

	phone = new QLineEdit;
	phone->setMaximumWidth(450);

	address = new QLineEdit;
	address->setMaximumWidth(450);

	city = new QLineEdit;
	city->setMaximumWidth(450);

	country = new QLineEdit;
	country->setMaximumWidth(450);

	postalCode = new QLineEdit;
	postalCode->setMaximumWidth(450);

	// sign up button
	signUp = new QPushButton;
	signUp->setText("Sign Up");

	
	formLayout->addRow(userLabel, userName);
	formLayout->addRow(passLabel, password);
	formLayout->addRow(fnLabel, firstName);
	formLayout->addRow(lnLabel, lastName);
	formLayout->addRow(emailLabel, email);
	formLayout->addRow(phoneLabel, phone);
	formLayout->addRow(addLabel, address);
	formLayout->addRow(cityLabel, city);
	formLayout->addRow(ctryLabel, country);
	formLayout->addRow(pcLabel, postalCode);
	formLayout->addRow(emptyLabel, signUp);	
	
	formLayout->setLabelAlignment(Qt::AlignLeft); // align text left for labels
	
	centreForm->setLayout(formLayout);
	layout->addWidget(space1);
	layout->addWidget(centreForm);
	layout->addWidget(space2);
	this->setLayout(layout);
	
	//QWidget *formContent = new QWidget;
	//formContent->setLayout(formLayout);
	
	//layout->addWidget(space1); 
	//layout->addWidget(formContent);
	//layout->addWidget(space2);
	
	
	this->setLayout(layout);
	
	connect(signUp, SIGNAL(clicked()), this, SLOT(signupClicked()));
}

void RegisterLayout::signupClicked()
{
   if(userName -> text() != "" && password -> text() != "" && firstName -> text() != "" && lastName -> text() != "" && email -> text() != "" && phone -> text() != "" && address -> text() != "" && city -> text() != "" && country -> text() != "" && postalCode -> text() != "")
   {
	u->setUsername(userName->text().toStdString());
	
	u->setPassword(password->text().toStdString());
	
	u->setFirstName(firstName->text().toStdString());
	
	u->setLastName(lastName->text().toStdString());
	
	u->setEmail(email->text().toStdString());
	
	u->setPhone(phone->text().toStdString());
	
	u->setAddress(address->text().toStdString());
	
	u->setCity(city->text().toStdString());
	
	u->setCountry(country->text().toStdString());
	
	u->setPostalCode(postalCode->text().toStdString());
	uh->addUser(u);
	
	QMessageBox successBox;
	successBox.information(0, "Success","Registered successfully! You can now log in!");
	successBox.setFixedSize(500,200);
	successBox.exec();
	
   }
   else
	{
		QMessageBox msgBox;
        msgBox.critical(0,"Error","Need to fill in all the fields!s");
        msgBox.setFixedSize(500,200);
		msgBox.exec();
	}
}

RegisterLayout::~RegisterLayout()
{
	
}
